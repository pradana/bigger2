import { createApp } from 'vue'
import App from './App.vue'
import style from './style/admin.css'
import bootstrap from 'bootstrap/dist/css/bootstrap.css'
import bootstrapjs from 'bootstrap/dist/js/bootstrap.js'
import mdi from './style/mdi-icons/css/materialdesignicons.css'


createApp(App).use(style, bootstrap, bootstrapjs, mdi).mount('#app')
